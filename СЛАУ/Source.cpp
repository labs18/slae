//#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ifstream cin("input");
ofstream cout("output");

vector<double> gaus(vector<vector<double>> matrix, int n)
{
	for (size_t i = 0; i < n - 1; i++)
		for (size_t j = i + 1; j < n; j++)
		{
			double koef = -matrix[j][i] / matrix[i][i];
			for (size_t k = i; k < n + 1; k++)
				matrix[j][k] += matrix[i][k] * koef;

			cout << endl;
			for (vector<double>& arr : matrix)
			{
				for (double& i : arr)
					cout << fixed << i << ' ';
				cout << endl;
			}
			cout << endl;

		}

	for (int i = n - 1; i > 0; i--)
		for (int j = i - 1; j >= 0; j--)
		{
			double koef = -matrix[j][i] / matrix[i][i];
			for (int k = n; k >= i; k--)
				matrix[j][k] += matrix[i][k] * koef;

			cout << endl;
			for (vector<double>& arr : matrix)
			{
				for (double& i : arr)
					cout << fixed << i << ' ';
				cout << endl;
			}
			cout << endl;

		}

	vector<double> answer(n);
	for (size_t i = 0; i < n; i++)
		answer[i] = matrix[i][n] / matrix[i][i];
	
	return answer;
}

int main()
{
	//for (int n; ; system("cls"))
	{
		int n;
		cin >> n;
		if (!n) return 0;

		vector<vector<double>> matrix(n, vector<double>(n + 1));

		for (vector<double>& arr : matrix)
			for (double& i : arr)
				cin >> i;
		int j = 1;
		for (double i : gaus(matrix, n))
			cout << fixed << i << '\n';
	}
	return 0;
}